import java.util.LinkedList;
import java.util.Queue;

public class DbConnection {

    private static Queue<DbConnection> pool = new LinkedList<>();

    private DbConnection() {
        // Privater Konstruktor verhindert die externe Instanziierung
    }

    public static DbConnection getConnection() {
        if (pool.isEmpty()) {
            DbConnection newConnection = new DbConnection();
            pool.offer(newConnection);
            return newConnection;
        } else {
            return pool.poll();
        }
    }

    public static void releaseConnection(DbConnection connection) {
        pool.offer(connection);
    }
}

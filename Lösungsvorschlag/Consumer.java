public class Consumer {

    public void useDatabase() {
        DbConnection connection = DbConnection.getConnection();
        
        // Verwendung der Verbindung
        
        DbConnection.releaseConnection(connection);
    }

    public static void main(String[] args) {
        Consumer consumer = new Consumer();
        consumer.useDatabase();
    }
}

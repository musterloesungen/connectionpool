### Aufgabenstellung
Entwickeln Sie ein Java-Programm, das simuliert, eine Datenbankverbindung zu verwalten. 

### Klasse `DbConnection`
- Implementieren Sie eine Klasse `DbConnection`, die die Verbindung zu einer Datenbank repräsentiert.
- Die Klasse soll einen privaten Konstruktor haben, um die Erstellung von Objekten durch andere Klassen zu verhindern.
- Implementieren Sie eine statische Methode `getConnection`, die eine Instanz von `DbConnection` zurückgibt. Wenn bereits eine Verbindung im Pool existiert, soll diese zurückgegeben werden. Wenn keine Verbindung vorhanden ist, soll eine neue erstellt und dem Pool hinzugefügt werden.
- Implementieren Sie eine statische Methode `releaseConnection`, die eine Verbindung zum Pool zurückgibt.

### Verbindungspool
- Implementieren Sie einen Verbindungspool, der intern in der Klasse `DbConnection` verwaltet wird. 
- Der Pool kann als eine statische Liste oder eine Queue von `DbConnection`-Objekten implementiert werden.
- Die Methode `getConnection` soll eine Verbindung aus dem Pool entnehmen (wenn eine vorhanden ist) oder eine neue Verbindung erstellen und dem Pool hinzufügen (wenn keine vorhanden ist).
- Die Methode `releaseConnection` soll eine Verbindung zum Pool zurückgeben.

### Verwendung der `DbConnection` Klasse
- Erstellen Sie eine oder mehrere Consumer-Klassen, die die `DbConnection` Klasse verwenden, um eine Datenbankverbindung zu erhalten und wieder freizugeben.

### Anforderungen
- Das Programm soll mehrere Verbindungsanfragen verarbeiten können, ohne dass dabei neue `DbConnection`-Objekte erzeugt werden, wenn bereits Verbindungen im Pool vorhanden sind.
